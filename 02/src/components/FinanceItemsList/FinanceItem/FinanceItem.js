import React from 'react';
import './FinanceItem.css';

const FinanceItem = ({index, name, cost, removeItem}) => {
  return (
    <div className="FinanceItem">
      <div className="FinanceItemName">{name}</div>
      <div className="FinanceItemCost">{cost} KGS</div>
      <button className="RemoveItemBtn" onClick={() => removeItem(index)}>X</button>
    </div>
  );
};

export default FinanceItem;
