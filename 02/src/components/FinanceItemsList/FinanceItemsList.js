import React from 'react';
import './FinanceItemsList.css';

import FinanceItem from './FinanceItem/FinanceItem';
import TotalCost from "./TotalCost/TotalCost";

const FinanceItemsList = ({financeItems, removeFinanceItem, totalCost}) => {
  const financeItemsArray = financeItems.reduce((acc, item) => {
    acc.push(
      <FinanceItem
        key={acc.length}
        index={acc.length}
        name={item.name}
        cost={item.cost}
        removeItem={removeFinanceItem}
      />
    );
    return acc;
  }, []);

  return (
    <div className="FinanceItemsList">
      {financeItemsArray}
      <TotalCost totalCost={totalCost}/>
    </div>
  );
};

export default FinanceItemsList;
