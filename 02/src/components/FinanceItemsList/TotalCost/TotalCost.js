import React from 'react';
import './TotalCost.css'

const TotalCost = ({totalCost}) => {
  return (
    <div className="TotalCost">
      Total spent: {totalCost} KGS
    </div>
  );
};

export default TotalCost;
