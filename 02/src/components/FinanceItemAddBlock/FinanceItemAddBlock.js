import React from 'react';
import './FinanceItemAddBlock.css';

const FinanceItemAddBlock = ({currentItemName, onItemNameChange, currentItemCost, onItemCostChange, addFinanceItem}) => {
  return (
    <form className="FinanceItemAddBlock" onSubmit={event => addFinanceItem(event, 'test')}>
      <input
        type="text"
        placeholder="Item name"
        value={currentItemName}
        onChange={event => onItemNameChange(event)}
        required={true}
      />
      <input
        type="number"
        placeholder="Cost"
        value={currentItemCost}
        onChange={event => onItemCostChange(event)}
        required={true}
      />
      <label>KGS</label>
      <button>Add</button>
    </form>
  );
};

export default FinanceItemAddBlock;
