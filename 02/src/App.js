import React, { Component } from 'react';
import './App.css';
import FinanceItemAddBlock from "./components/FinanceItemAddBlock/FinanceItemAddBlock";
import FinanceItemsList from "./components/FinanceItemsList/FinanceItemsList";

class App extends Component {
  state = {
    financeItems: [],
    currentAddItemName: '',
    currentAddItemCost: ''
  };

  addFinanceItemHandler = (event) => {
    event.preventDefault();

    let financeItems = [...this.state.financeItems];

    financeItems.push({
      name: this.state.currentAddItemName,
      cost: this.state.currentAddItemCost
    });

    this.setState({
      financeItems,
      currentAddItemName: '',
      currentAddItemCost: ''
    });
  };

  removeFinanceItemHandler = (index) => {
    let financeItems = [...this.state.financeItems];
    financeItems.splice(index, 1);

    this.setState({financeItems});
  };

  onItemNameChangeHandler = (event) => {
    this.setState({currentAddItemName: event.target.value});
  };

  onItemCostChangeHandler = (event) => {
    let value = parseInt(event.target.value) || '';
    this.setState({currentAddItemCost: value});
  };

  render() {
    const totalCost = this.state.financeItems.reduce((acc, item) => {
      return acc + parseInt(item.cost);
    }, 0);

    return (
      <div className="App">
        <FinanceItemAddBlock
          currentItemName={this.state.currentAddItemName}
          onItemNameChange={this.onItemNameChangeHandler}
          currentItemCost={this.state.currentAddItemCost}
          onItemCostChange={this.onItemCostChangeHandler}
          addFinanceItem={this.addFinanceItemHandler}
        />
        <FinanceItemsList
          financeItems={this.state.financeItems}
          removeFinanceItem={this.removeFinanceItemHandler}
          totalCost={totalCost}
        />
      </div>
    );
  }
}

export default App;
