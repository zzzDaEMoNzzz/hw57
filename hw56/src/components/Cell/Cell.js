import React from 'react';
import './Cell.css';

const Cell = props => {
  const CellStyle = {
    width: props.size,
    height: props.size
  };

  const CellClasses = ['Cell'];
  if (props.isActive) {
    CellClasses.push('active');
  } else if (props.hasItem) {
    CellClasses.push('hasItem');
  }

  const onCellClick = () => {
    if (props.isActive && props.isGameActive) {
      props.deactivateCell();
      props.increaseTries();

      if (props.hasItem) {
        alert('Congratulations, you found item.');
        props.stopGame();
      }
    }
  };

  return (
    <div className="CellWrapper">
      <button className={CellClasses.join(' ')} style={CellStyle} onClick={onCellClick} />
    </div>
  );

};

export default Cell;
