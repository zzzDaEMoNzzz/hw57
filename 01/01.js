const tasks = [
  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const frontendTimeSpent = tasks.reduce((acc, task) => {
  if (task.category === 'Frontend') {
    return acc + task.timeSpent;
  }
  return acc;
}, 0);

console.log('Frontend time spent:', frontendTimeSpent);

const bugTimeSpent = tasks.reduce((acc, task) => {
  if (task.type === 'bug') {
    return acc + task.timeSpent;
  }
  return acc;
}, 0);

console.log('Bug time spent:', bugTimeSpent);

const UITimeSpent = tasks.reduce((acc, task) => {
  if (task.title.includes('UI')) {
    return acc + task.timeSpent;
  }
  return acc;
}, 0);

console.log('UI tasks time spent:', UITimeSpent);

const tasksInCategory = tasks.reduce((acc, task) => {
  const category = task.category;

  if (category in acc) {
    acc[category]++;
  } else {
    acc[category] = 1;
  }

  return acc;
}, {});

console.log(tasksInCategory);

const tasksWithTimeSpentMoreThen4 = tasks.reduce((acc, task) => {
  if (task.timeSpent > 4) {
    acc.push({'title': task.title, 'category': task.category})
  }

  return acc;
}, []);

console.log('Tasks with time spent > 4:\n', tasksWithTimeSpentMoreThen4);